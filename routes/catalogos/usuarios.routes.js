module.exports = app => {
  const usuarios = require('../../controllers/catalogos/usuarios.controllers')
  /*
    Todas las rutas aquí creadas estarán dirigidas hacia los usuarios
  */ 

  app.post("/usuarios.add",          usuarios.addUsuario);
  app.post("/usuarios.foto",         usuarios.addFotoUsuario);
  app.put("/usuarios.update/:id",    usuarios.updateUsuario);
  app.get("/usuarios.activos",       usuarios.getUsuariosActivos);
  app.post("/usuario.acceso",        usuarios.validarAcceso);

  // Adminsitración
  app.get("/usuarios.get",           usuarios.getUsuarios);
  app.get("/usuarios.terapeutas",    usuarios.getTerapeutas);
  app.get("/usuarios.pacientes",     usuarios.getPacientes);
  app.get("/usuarios.prospectos",    usuarios.getProspectos);

  // ESCUELA
  app.get("/usuarios.tutores",       usuarios.getTutores);
  app.get("/usuarios.maestros",      usuarios.getMaestros);
  app.get("/usuarios.alumnos",       usuarios.getAlumnos);
  app.post("/alumnos.add",           usuarios.addAlumno);
  app.put("/alumnos.update/:id",     usuarios.updateAlumno);

  


};