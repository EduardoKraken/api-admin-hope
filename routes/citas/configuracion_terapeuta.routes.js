module.exports = app => {
  const configuracion_terapeuta = require('../../controllers/citas/configuracion_terapeuta.controllers')
  /*
    Todas las rutas aquí creadas estarán dirigidas hacia los usuarios
  */ 

  app.post("/configuracion.terapeuta.add",          configuracion_terapeuta.addConfiguracionTerapeuta);
  app.get("/configuracion.terapeuta.get",           configuracion_terapeuta.getConfiguracionTerapeuta);
  app.put("/configuracion.terapeuta.update/:id",    configuracion_terapeuta.updateConfiguracionTerapeuta);
  app.get("/configuracion.terapeuta.get/:id",       configuracion_terapeuta.getHorariosTerapeuta);

};