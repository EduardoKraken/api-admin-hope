const agenda_citas    = require("../../models/citas/agenda_citas.model.js");
const historial_cita  = require("../../models/citas/historial_cita.model.js");
const evidencia_cita  = require("../../models/citas/evidencia_cita.model.js");
const config          = require("../../config/index.js");
const md5             = require('md5');
const { v4: uuidv4 }  = require('uuid')


// AGENDAR UNA CITA
exports.addAgendaCita = async (req, res) => {
  try{

    // vALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!req.body || Object.keys(req.body).length === 0) {
      return res.status( 400 ).send({ message: 'El Contenido no puede estar vacio' });
    }
    
    let { id_terapeuta, id_paciente, fecha_cita, fecha_limite, hora_inicio, hora_final, periocidad, dias, idcoordinacion, idservicios, costo, repeticion } = req.body

    // Acomodemos los días, ya que están por posición
    for( const i in dias ){ dias[i] = parseInt(dias[i]) + 1 }

    // Sacar todas las fechas entre las fechas seleccionadas
    const fechasSeleccionadas = await agenda_citas.getFechasSeleccionadas( fecha_cita, fecha_limite ).then( response => response )

    console.log( fechasSeleccionadas )
    let inicial = 1
    let final   = 7

    // Agendemos primero las semanales
    if( periocidad == 'Semana' ){
      
      for( let i in fechasSeleccionadas ){

        // Desestrucutración de datos
        const { fecha, dia } = fechasSeleccionadas[i]

        if( dias.includes(dia) && ( parseInt( i ) + 1) >= inicial && ( parseInt( i ) + 1 ) <= final ){
          // Aquí, hay que agendar
          // AGENDAR LA CITA
          const payload = {
            id_terapeuta, id_paciente, fecha_cita: fecha, hora_inicio, hora_final, idcoordinacion, idservicios, costo
          }

          const agendaGenerada  = await agenda_citas.addAgendaCita( payload ).then( response => response )
        } 

        if( repeticion > 1 && dia == 7 && final == ( parseInt( i ) + 1 ) ){ 
          inicial += ( repeticion * 7 )
          final   += ( repeticion * 7 )
        }

      }

    }
    
    // // COMPRAR SI AL PACIENTE NO SE LE EMPALMAN LAS FECHAS
    // const agendaPaciente = await agenda_citas.agendaPaciente( id_paciente, fecha_cita, hora_inicio, hora_final ).then( response => response )

    // // VALIDAR INFORMACION
    // if( agendaPaciente ){ return res.status( 400 ).send({ message: 'Se empalma fecha y hora del paciente' });  }

    // // COMPRAR SI AL PACIENTE NO SE LE EMPALMAN LAS FECHAS
    // const agendaTerapeuta = await agenda_citas.agendaTerapeuta( id_terapeuta, fecha_cita, hora_inicio, hora_final ).then( response => response )
    
    // // VALIDAR INFORMACION
    // if( agendaTerapeuta ){ return res.status( 400 ).send({ message: 'Se empalma fecha y hora del terapeuta' });  }


    // MENSAJE DE RESPUESTA AL Agenda
    res.send({ message: 'Agenda generada correctamente' })

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } )
  }
}

// OBTENER TODAS LAS CITAS EN GENERAL
exports.getAgendaCitas = async (req, res) => {
  try{
    
    // OBTENER LA AGENDA DE TODOS 
    const agendasAll  = await agenda_citas.getAgendaCitas( ).then( response => response )

    // CONSULTAR HISTORIAL DE LA AGENDA
    let idAgenda = agendasAll.map((registro)=> { return registro.idagenda_citas })
    idAgenda = idAgenda.length ? idAgenda : [0]

    // CONSULTAR EL HISTORIAL 
    const historialAgenda = await historial_cita.getHistorialCita( idAgenda ).then( response => response )

    // CONSULTAR LAS EVIDENCIAS
    const evidenciaCita   = await evidencia_cita.getEvidenciaCitas( idAgenda ).then( response => response )


    for( const i in agendasAll ){
      const { idagenda_citas } = agendasAll[i]

      agendasAll[i]['historial']  = historialAgenda.filter( el => { return el.idagenda_cita == idagenda_citas })
      agendasAll[i]['evidencias'] = evidenciaCita.filter( el => { return el.idagenda_cita == idagenda_citas })
    }

    // RESPUESTA AL Agenda
    res.send( agendasAll )

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } )
  }
};

exports.getAgendaCitasTerapueta = async (req, res) => {
  try{

    const { id } = req.params
    
    // OBTENER LA AGENDA DE TODOS 
    let agendasAll  = await agenda_citas.getAgendaCitas( ).then( response => response )

    // CONSULTAR HISTORIAL DE LA AGENDA
    let idAgenda = agendasAll.map((registro)=> { return registro.idagenda_citas })
    idAgenda = idAgenda.length ? idAgenda : [0]

    // CONSULTAR EL HISTORIAL 
    const historialAgenda = await historial_cita.getHistorialCita( idAgenda ).then( response => response )

    // CONSULTAR LAS EVIDENCIAS
    const evidenciaCita   = await evidencia_cita.getEvidenciaCitas( idAgenda ).then( response => response )


    for( const i in agendasAll ){
      const { idagenda_citas } = agendasAll[i]

      agendasAll[i]['historial']  = historialAgenda.filter( el => { return el.idagenda_cita == idagenda_citas })
      agendasAll[i]['evidencias'] = evidenciaCita.filter( el => { return el.idagenda_cita == idagenda_citas })
    }

    agendasAll = agendasAll.filter( el => { return el.id_terapeuta == id  })

    // RESPUESTA AL Agenda
    res.send( agendasAll )

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } )
  }
};

// ACTUALIZAR O DAR DE BAJA UNA CITA
exports.updateAgendaCita = async (req, res) => {
  try{

    // Desestrucutramos los datos
    const { id } = req.params

    // vALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!req.body || Object.keys(req.body).length === 0 || !id ) {
      return res.status( 400 ).send({ message: 'El Contenido no puede estar vacio' });
    }

    // ACTUALIZAR LA AGENDA
    const agendaActualizada = await agenda_citas.updateAgendaCita( req.body, id ).then( response=> response )

    res.send({ message: 'Agenda actualizada correctamente' })

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } )
  }
};

// OBTENER TODAS LAS CITAS AGENDADAS ACTIVAS
exports.getAgendasCitasActivas = async (req, res) => {
  try{

    // OBTENER SOLO LAS AGENDAS ACTIVAS MENOS LOS ELIMINADOS
    const agendasAll  = await agenda_citas.getAgendasCitasActivas( ).then( response => response )

    // CONSULTAR HISTORIAL DE LA AGENDA
    let idAgenda = agendasAll.map((registro)=> { return registro.idagenda_citas })
    idAgenda = idAgenda.length ? idAgenda : [0]

    // CONSULTAR EL HISTORIAL 
    const historialAgenda = await historial_cita.getHistorialCita( idAgenda ).then( response => response )

    // CONSULTAR LAS EVIDENCIAS
    const evidenciaCita   = await evidencia_cita.getEvidenciaCitas( idAgenda ).then( response => response )

    for( const i in agendasAll ){
      const { idagenda_citas } = agendasAll[i]

      agendasAll[i]['historial']  = historialAgenda.filter( el => { return el.idagenda_cita == idagenda_citas })
      agendasAll[i]['evidencias'] = evidenciaCita.filter( el => { return el.idagenda_cita == idagenda_citas })
    }

    // RESPUESTA AL Agenda
    res.send( agendasAll )

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } )
  }
};


// OBTENER LA AGENDA DE UN USUARIO
exports.getAgendaCitasUsuario = async (req, res) => {
  try{

    // Desestrucutramos los datos
    const { id } = req.params

    // OBTENER LA AGENDA DE TODOS 
    const agendasAll  = await agenda_citas.getAgendaCitas( ).then( response => response )

    // CONSULTAR HISTORIAL DE LA AGENDA
    let idAgenda = agendasAll.map((registro)=> { return registro.idagenda_citas })
    idAgenda = idAgenda.length ? idAgenda : [0]

    // CONSULTAR EL HISTORIAL 
    const historialAgenda = await historial_cita.getHistorialCita( idAgenda ).then( response => response )

    // CONSULTAR LAS EVIDENCIAS
    const evidenciaCita   = await evidencia_cita.getEvidenciaCitas( idAgenda ).then( response => response )


    for( const i in agendasAll ){
      const { idagenda_citas } = agendasAll[i]

      agendasAll[i]['historial']  = historialAgenda.filter( el => { return el.idagenda_cita == idagenda_citas })
      agendasAll[i]['evidencias'] = evidenciaCita.filter( el => { return el.idagenda_cita == idagenda_citas })
    }

    const respuesta = agendasAll.filter( el => { return el.id_paciente == id })

    // RESPUESTA AL Agenda
    res.send( respuesta )


  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } )
  }
};

exports.deleteCitas = async (req, res) => {
  try{

    // Desestrucutramos los datos
    const { tipoDelete, idcoordinacion, id_terapeuta, id_paciente, hora_inicio, hora_final, idagenda_citas, fecha_cita, idservicios } = req.body

    // Validar el tipo de delete, 1 = solo una cita
    if( tipoDelete == 1 ){
      // eliminar solo esa cita
      const agendaActualizada = await agenda_citas.deleteCita( idagenda_citas ).then( response=> response )
    }

    if( tipoDelete == 2 ){

      // Consultar las citas mayores e igual a la fecha actual
      const citasMayoActual = await agenda_citas.citasMayoActual( id_terapeuta, id_paciente, hora_inicio, hora_final, fecha_cita, idservicios ).then( response => response )
      
      for( const i in citasMayoActual ){

        // eliminar solo esa cita
        const agendaActualizada = await agenda_citas.deleteCita( citasMayoActual[i].idagenda_citas ).then( response=> response )
      }
    }

    if( tipoDelete == 3 ){

      // Consultar las citas mayores e igual a la fecha actual
      const citasMayoActual = await agenda_citas.citasTodas( id_terapeuta, id_paciente, hora_inicio, hora_final, idservicios ).then( response => response )
      
      
      for( const i in citasMayoActual ){

        // eliminar solo esa cita
        const agendaActualizada = await agenda_citas.deleteCita( citasMayoActual[i].idagenda_citas ).then( response=> response )
      }
    }

    // RESPUESTA AL Agenda
    res.send({ message: 'Datos actualizados correctamente'})


  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } )
  }
};