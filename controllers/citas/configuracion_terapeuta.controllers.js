const usuarios                = require("../../models/catalogos/usuarios.model.js");
const configuracion_terapeuta = require("../../models/citas/configuracion_terapeuta.model.js");
const config                  = require("../../config/index.js");
const md5                     = require('md5');
const { v4: uuidv4 }          = require('uuid')


// AGREGAR LA CONFIGURACION DE UN HORARIO DE UN TERAPEUTA
exports.addConfiguracionTerapeuta = async (req, res) => {
  try{

    // vALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!req.body || Object.keys(req.body).length === 0) {
      return res.status( 400 ).send({ message: 'El Contenido no puede estar vacio' });
    }

    const { id_usuario, dias } = req.body

    for( const i in dias ){
      const { dia, hora_inicio, hora_fin } = dias[i]

      // Validar si ya existe un horariop
      const existeHorario = await configuracion_terapeuta.existeHorario( id_usuario, dia ).then( response => response )

      if( existeHorario ){
        const { idconfiguracion_terapeuta } = existeHorario
        // Actualizar 
        const horarioActualizado = await configuracion_terapeuta.updateConfiguracionTerapeuta( dia, hora_inicio, hora_fin, idconfiguracion_terapeuta ).then( response=> response )
      }else{
        // AGREGAR LA CONFIGURACION
        const horarioAgregado  = await configuracion_terapeuta.addConfiguracionTerapeuta( id_usuario, dia, hora_inicio, hora_fin ).then( response => response )
      }
    }

    // MENSAJE DE RESPUESTA AL HORARIO
    res.send({ message: 'Horario generado correctamente' })

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } )
  }
}

// OBTENER TODAS LOS HORARIOS
exports.getConfiguracionTerapeuta = async (req, res) => {
  try{
    
    // OBTENER TODAS LOS HORARIOS MENOS LOS ELIMINADOS
    const horariosAll  = await configuracion_terapeuta.getConfiguracionTerapeuta( ).then( response => response )

    // RESPUESTA AL HORARIO
    res.send( horariosAll )

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } )
  }
};

// ACTUALIZAR EL HORARIO O BIEN ELIMINARLO
exports.updateConfiguracionTerapeuta = async (req, res) => {
  try{

    // Desestrucutramos los datos
    const { id } = req.params

    // vALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!req.body || Object.keys(req.body).length === 0 || !id ) {
      return res.status( 400 ).send({ message: 'El Contenido no puede estar vacio' });
    }

    // una vez enviado, hay que actualizar la contraseñ
    const horarioActualizado = await configuracion_terapeuta.updateConfiguracionTerapeuta( req.body, id ).then( response=> response )

    res.send({ message: 'Horario actualizado correctamente' })

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } )
  }
};

// OBTENER SOLO LOS HORARIOS ACTIVOS
exports.getHorariosTerapeuta = async (req, res) => {
  try{

    // Desestrucutramos los datos
    const { id } = req.params
    
    // OBTENER SOLO LOS HORARIOS ACTIVOS MENOS LOS ELIMINADOS
    const horariosAll  = await configuracion_terapeuta.getHorariosTerapeuta( id ).then( response => response )

    // RESPUESTA AL HORARIO
    res.send( horariosAll )

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } )
  }
};

