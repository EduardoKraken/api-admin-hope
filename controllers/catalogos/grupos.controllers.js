const grupos   = require("../../models/catalogos/grupos.model.js");
const config   = require("../../config/index.js");
const md5      = require('md5');

// AGREGAR COORDINACION
exports.addGrupo = async (req, res) => {
  try{

    // vALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!req.body || Object.keys(req.body).length === 0) {
      return res.status( 400 ).send({ message: 'El Contenido no puede estar vacio' });
    }
    
    // AGREGAR EL COORDINACION
    const CoordinacionAgregada  = await grupos.addGrupo( req.body ).then( response => response )

    // MENSAJE DE RESPUESTA AL USUARIO
    res.send({ message: 'Grupos generado correctamente', CoordinacionAgregada })

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } )
  }
}

// OBTENER TODAS LAS COORDINACIONES
exports.getGrupos = async (req, res) => {
  try{
    
    // OBTENER TODAS LAS COORDINACIONES MENOS LAS ELIMINADAS
    const gruposAll  = await grupos.getGrupos( ).then( response => response )

    // RESPUESTA AL USUARIO
    res.send( gruposAll )

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } )
  }
};

// ACTUALIZAR LA COORDINACION O BIEN ELIMINARLA
exports.updateGrupos = async (req, res) => {
  try{
    // Desestrucutramos los datos
    const { id } = req.params

    // vALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!req.body || Object.keys(req.body).length === 0 || !id ) {
      return res.status( 400 ).send({ message: 'El Contenido no puede estar vacio' });
    }

    // una vez enviado, hay que actualizar la contraseñ
    const grupoActualizado = await grupos.updateGrupos( req.body, id ).then( response=> response )

    res.send({ message: 'Coordinación actualizada correctamente' })

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } )
  }
};

// OBTENER SOLO LOS COORDINACIONES ACTIVAS
exports.getGruposActivos = async (req, res) => {
  try{

    // OBTENER TODOS LOS COORDINACIONES MENOS LOS ELIMINADOS
    const gruposAll  = await grupos.getGruposActivos( ).then( response => response )

    // RESPUESTA AL USUARIO
    res.send( gruposAll )

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } )
  }
};


exports.getGruposAlumnos = async (req, res) => {
  try{

  	const { idgrupos } = req.body

    // OBTENER TODOS LOS COORDINACIONES MENOS LOS ELIMINADOS
    const alumnosGrupo  = await grupos.getGruposAlumnos( idgrupos ).then( response => response )

    // RESPUESTA AL USUARIO
    res.send( alumnosGrupo )

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } )
  }
};


exports.addAlumnoGrupo = async (req, res) => {
  try{

    // vALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!req.body || Object.keys(req.body).length === 0) {
      return res.status( 400 ).send({ message: 'El Contenido no puede estar vacio' });
    }
    
    // AGREGAR EL COORDINACION
    const alumnoAgregado  = await grupos.addAlumnoGrupo( req.body ).then( response => response )

    // MENSAJE DE RESPUESTA AL USUARIO
    res.send({ message: 'Grupos generado correctamente', alumnoAgregado })

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } )
  }
}


exports.getGruposTeacher = async (req, res) => {
  try{

    const { id } = req.params

    // OBTENER TODOS LOS COORDINACIONES MENOS LOS ELIMINADOS
    const alumnosGrupo  = await grupos.getGruposTeacher( id ).then( response => response )

    // RESPUESTA AL USUARIO
    res.send( alumnosGrupo )

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } )
  }
};


exports.getAsistencias = async (req, res) => {
  try{

    const { idgrupos, fecha } = req.body

    // OBTENER TODOS LOS COORDINACIONES MENOS LOS ELIMINADOS
    const alumnosGrupo  = await grupos.getGruposAlumnos( idgrupos ).then( response => response )

    // // OBTENER TODOS LOS COORDINACIONES MENOS LOS ELIMINADOS
    const asistencias  = await grupos.getAsistencias( idgrupos, fecha ).then( response => response )

    for( const i in alumnosGrupo ){
      // VER SI EXISTE EL ALUMNO CON ASISTENCIA

      const existeAsistencia = asistencias.find( el => el.idalumnos == alumnosGrupo[i].idalumnos )

      if( existeAsistencia ){
        alumnosGrupo[i]['asistencia']    = existeAsistencia.asistencia
        alumnosGrupo[i]['idasistencias'] = existeAsistencia.idasistencias
        alumnosGrupo[i]['animo']         = existeAsistencia.animo
        alumnosGrupo[i]['banio']         = existeAsistencia.banio
        alumnosGrupo[i]['comida']        = existeAsistencia.comida
        alumnosGrupo[i]['trabajo']       = existeAsistencia.trabajo
        alumnosGrupo[i]['detallebanio']  = existeAsistencia.detallebanio
      }else{
        alumnosGrupo[i]['asistencia']    = 0
        alumnosGrupo[i]['idasistencias'] = null
      }

    }

    // RESPUESTA AL USUARIO
    res.send( alumnosGrupo )

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } )
  }
};

exports.addAsistencias = async (req, res) => {
  try{

    const { idgrupos, fecha, asistencias } = req.body

    const filterAsistencias = asistencias.filter( el => { return el.idasistencias })

    for( const i in filterAsistencias ){
      const { idasistencias, asistencia, idalumnos } = filterAsistencias[i]

      if(idasistencias){
        const updateAsistencia = await grupos.updateAsistencia( idasistencias, asistencia ).then( response => response )
      }else{
        // Esto, por quel momento de recibir el arreglo, estamos mandanodo el que usamos para mostrar los tonos de las asistencias
        if( idalumnos ){
          const addASistenciaGrupo = await grupos.addASistenciaGrupo( idgrupos, idalumnos, fecha, asistencia ).then( response => response )
        }
      }
    }

    // RESPUESTA AL USUARIO
    res.send( { message: 'Datos actualizados correctamente'} )

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } )
  }
};


exports.addReporte = async (req, res) => {
  try{

    const { id_asistencia, idalumnos, idgrupos, fecha, animo, banio, comida, trabajo, detalleBanio } = req.body

    // Validar si ya existe un reporte para esa fecha
    const existeReporte = await grupos.existeReporte( idalumnos, idgrupos, fecha ).then( response => response )

    // Validamos la existencia del reporte
    if( existeReporte ){

      const { idreporte_alumnos } = existeReporte 

      // Existe! Actualizar! 
      const updateReporteAlumno = await grupos.updateReporteAlumno( animo, banio, comida, trabajo, detalleBanio, idreporte_alumnos ).then( response => response )    

    }else{

      // No existe! Agregar!
      const addReporteAlumno = await grupos.addReporteAlumno( idalumnos, idgrupos, fecha, animo, banio, comida, trabajo, detalleBanio ).then( response => response )    

    }

    // RESPUESTA AL USUARIO
    res.send( { message: 'Datos actualizados correctamente'} )

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } )
  }
};

exports.gerReporteAlumno = async (req, res) => {
  try{

    const { idalumnos, idgrupos, fecha } = req.body

    // Validar si ya existe un reporte para esa fecha
    const existeReporte = await grupos.existeReporte( idalumnos, idgrupos, fecha ).then( response => response )

    // Validamos la existencia del reporte
    if( existeReporte ){

      res.send( existeReporte )
      
    }else{

      res.status( 500 ).send({ message: 'No existe reporte '})

    }

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } )
  }
};

exports.getAlumnosTutor = async( req, res ) =>{

  try{

    // Sacamos el id del tutor
    const { id } = req.params   

    // Obtner los alumnos del tutor
    const alumnosTutor = await grupos.getAlumnosTutor( id ).then( response => response )

    // Obtener los ids de los alumnos del tutor
    let mapAlumnos = alumnosTutor.map(registro => registro.idalumnos);

    mapAlumnos = mapAlumnos.length ? mapAlumnos : [0]

    // obtener los grupos de los alumnos del tutor
    const gruposAlumnos = await grupos.getGruposAlumno( mapAlumnos ).then( response => response )

    // Juntar los grupos del alumno
    for( const i in alumnosTutor ){

      // Sacar el id del alumno
      const { idalumnos } = alumnosTutor[i]

      // filtarmos los grupos por alumno
      const gruposAlumno = gruposAlumnos.filter( el => el.idalumnos == idalumnos )

      // Agregamos
      alumnosTutor[i]['grupos'] = gruposAlumno

    }

    // Respondemos al servidor
    res.send( alumnosTutor )

  }catch( error ){

    return res.status( 500 ).send({ message: error ? error.message : 'Error en el servidor, comunícate con sistemas' })

  }

}