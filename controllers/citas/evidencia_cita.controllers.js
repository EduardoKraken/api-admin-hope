const evidencia_cita  = require("../../models/citas/evidencia_cita.model.js");
const config          = require("../../config/index.js");
const md5             = require('md5');
const { v4: uuidv4 }  = require('uuid')

// AGENDAR UNA CITA
exports.addEvidenciaCita = async ( req, res) => {
  try{

    // desestrucutramos los arvhios cargados
    let { file  } = req.files

    if(!file.length){
      file = [file]
    }

    let nombreArchivos = []

    for( const i in file ){

      const nombreSplit = file[i].name.split('.')
      
      // Obtener la extensión del archivo 
      const extensioArchivo = nombreSplit[ nombreSplit.length - 1 ]
      
      let extensiones = [ 'mp4', 'MOV', 'AVI', 'WMV', 'mp3', 'wav', 'wma', 'BMP', 'GIF', 'jpg', 'jpeg', 'png', 'webp', 'pdf' ]
      let ruta        = ''

      // modificar el nombre del archivo con un uuid unico
      const nombreUuid = uuidv4() + '.' + extensioArchivo
      
      // Videos
      if( [ 'mp4', 'MOV', 'AVI', 'WMV' ].includes( extensioArchivo ) ){
        ruta        = './../../hope-videos/' + nombreUuid
      }

      // Audios
      if( [ 'mp3', 'wav', 'wma' ].includes( extensioArchivo ) ){
        ruta        = './../../hope-audios/' + nombreUuid
      }

      // Imapgenes
      if( [ 'BMP', 'GIF', 'jpg', 'jpeg', 'png', 'webp' ].includes( extensioArchivo ) ){
        ruta        = './../../hope-imagenes/' + nombreUuid
      }

      // Archvios
      if( [ 'pdf' ].includes( extensioArchivo ) ){
        ruta        = './../../hope-archivos/' + nombreUuid
      }

      nombreArchivos.push( {archivo: nombreUuid, extension: extensioArchivo} )

      // validar que la extensión del archivo recibido sea valido
      if( !extensiones.includes( extensioArchivo) )
        return res.status( 400 ).send({message: `Hola, la estensión: ${ extensioArchivo } no es valida, solo se aceptan: ${ extensiones }`})
      
      file[i].mv(`${ruta}`, async ( err ) => {
        try {
          if( err )
            return res.status( 400 ).send({ message: err })
        }catch( error ){
          res.status( 500 ).send({ message: error ? error.message : 'Error al subir la imagen' })
        }
      })
    }
    
    res.send( nombreArchivos );
    
  }catch( error ){
    res.status( 500 ).send({ message: 'Error al subir la imagen' })
  }
}

// ACTUALIZAR O DAR DE BAJA UNA CITA
exports.updateEvidenciaCita = async (req, res) => {
  try{

    // Desestrucutramos los datos
    const { id } = req.params

    // vALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!req.body || Object.keys(req.body).length === 0 || !id ) {
      return res.status( 400 ).send({ message: 'El Contenido no puede estar vacio' });
    }

    // ACTUALIZAR LA AGENDA
    const agendaActualizada = await agenda_citas.updateAgendaCita( req.body, id ).then( response=> response )

    res.send({ message: 'Agenda actualizada correctamente' })

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } )
  }
};

