module.exports = app => {
  const coordinaciones = require('../../controllers/catalogos/coordinaciones.controllers')
  /*
    Todas las rutas aquí creadas estarán dirigidas hacia los coordinaciones
  */ 

  app.post("/coordinaciones.add",          coordinaciones.addCoordinacion);
  app.get("/coordinaciones.get",           coordinaciones.getCoordinaciones);
  app.put("/coordinaciones.update/:id",    coordinaciones.updateCoordinacion);
  app.get("/coordinaciones.activos",       coordinaciones.getCoordinacionesActivas);
};