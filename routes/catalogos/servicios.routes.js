module.exports = app => {
  const servicios = require('../../controllers/catalogos/servicios.controllers')
  /*
    Todas las rutas aquí creadas estarán dirigidas hacia los servicios
  */ 

  app.post("/servicios.add",          servicios.addServicios);
  app.get("/servicios.get",           servicios.getServicios);
  app.put("/servicios.update/:id",    servicios.updateServicios);
  app.get("/servicios.activos",       servicios.getServiciosActivos);
};