const dotenv = require('dotenv').config();

module.exports = {
  ENV: process.env.ENV,
  PORT: process.env.PORT || 3001,
}
