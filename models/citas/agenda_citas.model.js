const { result } = require("lodash");
const sqlERP     = require("../db.js");


//const constructor
const agenda_citas = (agenda_citas) => {};

// Agregar datos usuario
agenda_citas.addAgendaCita = ( c ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`INSERT INTO agenda_citas( id_terapeuta, id_paciente, fecha_cita, hora_inicio, hora_final, idcoordinacion, idservicios, costo ) VALUES( ?,?, ?, ?, ?, ?, ?, ? )`,
      [ c.id_terapeuta, c.id_paciente, c.fecha_cita, c.hora_inicio, c.hora_final, c.idcoordinacion, c.idservicios, c.costo ], (err, res) => {

      // VALIDAR ERRORES
      if (err) { return reject({ message: err.sqlMessage }); }

      // RESPUESTA AL CONTROLADOR
      return resolve({ id: res.insertId, ...c });
    });
  })
};


agenda_citas.getAgendaCitas = ( ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`SELECT a.idagenda_citas, a.id_terapeuta, a.id_paciente, a.fecha_cita, a.hora_inicio, a.hora_final, a.idcoordinacion,
      CONCAT(u.nombres, " ", u.apellido_paterno, " ", u.apellido_materno) AS paciente, s.servicio, a.costo, a.idservicios,
      CONCAT(u2.nombres, " ", u2.apellido_paterno, " ", u2.apellido_materno) AS terapeuta, a.idagenda_citas,
      u.idusuarios, CONCAT(u.nombres, " ", u.apellido_paterno, " ", u.apellido_materno) AS nombre_completo,
      c.color, c.coordinacion, a.estatus, CONCAT(a.fecha_cita, ' ', a.hora_inicio ) AS fecha_agenda, u.telefono,
      IF( DATEDIFF(DATE(CONCAT(a.fecha_cita, ' ', a.hora_inicio )), NOW()) > 0 , DATEDIFF(DATE(CONCAT(a.fecha_cita, ' ', a.hora_inicio )), NOW()),10000) AS restan 
      FROM agenda_citas a
      LEFT  JOIN coordinaciones c ON c.idcoordinaciones = a.idcoordinacion
      LEFT JOIN usuarios u ON u.idusuarios = a.id_paciente
      LEFT JOIN usuarios u2 ON u2.idusuarios = a.id_terapeuta
      LEFT JOIN servicios s ON s.idservicios = a.idservicios
      WHERE a.deleted = 0 AND a.estatus > 0
      ORDER BY IF( DATEDIFF(DATE(CONCAT(a.fecha_cita, ' ', a.hora_inicio )), NOW()) > 1 , DATEDIFF(DATE(CONCAT(a.fecha_cita, ' ', a.hora_inicio )), NOW()),10000);`, (err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res );
    });
  })
};

agenda_citas.updateAgendaCita = ( c, id ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(` UPDATE agenda_citas SET id_terapeuta= ?, id_paciente= ?, fecha_cita= ?, hora_inicio= ?, hora_final= ?, 
    	idcoordinacion= ?, estatus = ?, deleted = ? 
      WHERE idagenda_citas = ?`,
      [ c.id_terapeuta, c.id_paciente, c.fecha_cita, c.hora_inicio,c. hora_final, c.idcoordinacion, c.estatus, c.deleted, id],
      (err, res) => {
      // VALIDAR ERRORES
      if (err) { return reject({ message: err.sqlMessage }); }

      if (res.affectedRows == 0) { return reject({ message: 'DATO NO ENCONTRADO' }) }

      resolve({ id, ...c });
    })
  })
};

agenda_citas.getAgendasCitasActivas = ( ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`SELECT * FROM agenda_citas WHERE deleted = 0 AND estatus > 0;`,(err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res );
    });
  })
};


agenda_citas.getAgendaCitasUsuario = ( id_usuario ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`SELECT a.id_terapeuta, a.id_paciente, a.fecha_cita, a.hora_inicio, a.hora_final, a.idcoordinacion,
			CONCAT(u.nombres, " ", u.apellido_paterno, " ", u.apellido_materno) AS paciente,
			CONCAT(u2.nombres, " ", u2.apellido_paterno, " ", u2.apellido_materno) AS terapeuta, a.idagenda_citas,
			c.color, c.coordinacion, a.estatus FROM agenda_citas a
			LEFT  JOIN coordinaciones c ON c.idcoordinaciones = a.idcoordinacion
			LEFT JOIN usuarios u ON u.idusuarios = a.id_paciente
			LEFT JOIN usuarios u2 ON u2.idusuarios = a.id_terapeuta
			WHERE u.idusuarios = ?
      ORDER BY a.fecha_cita DESC;`,[ id_usuario ],(err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res );
    });
  })
};

agenda_citas.agendaPaciente = ( id_paciente, fecha_cita, hora_inicio, hora_final ) => {
  console.log( id_paciente, fecha_cita, hora_inicio, id_paciente, fecha_cita, hora_final )
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`SELECT * FROM agenda_citas
      WHERE id_paciente = ?
      AND fecha_cita = ?
      AND TIME(?) BETWEEN hora_inicio AND hora_final AND deleted = 0
      OR id_paciente = ?
      AND fecha_cita = ?
      AND TIME(?) BETWEEN hora_inicio AND hora_final AND deleted = 0;`,[ id_paciente, fecha_cita, hora_inicio, id_paciente, fecha_cita, hora_final ],(err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res[0] );
    });
  })
};

agenda_citas.agendaTerapeuta = ( id_terapeuta, fecha_cita, hora_inicio, hora_final ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`SELECT * FROM agenda_citas
      WHERE id_terapeuta = ?
      AND fecha_cita = ?
      AND TIME(?) BETWEEN hora_inicio AND hora_final AND deleted = 0
      OR id_terapeuta = ?
      AND fecha_cita = ?
      AND TIME(?) BETWEEN hora_inicio AND hora_final AND deleted = 0;`,[ id_terapeuta, fecha_cita, hora_inicio, id_terapeuta, fecha_cita, hora_final ],(err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res[0] );
    });
  })
};


agenda_citas.citaCumplida = ( id ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(` UPDATE agenda_citas SET estatus = 2 WHERE idagenda_citas = ?`, [ id ],
      (err, res) => {
      // VALIDAR ERRORES
      if (err) { return reject({ message: err.sqlMessage }); }

      if (res.affectedRows == 0) { return reject({ message: 'DATO NO ENCONTRADO' }) }

      resolve({ id  });
    })
  })
};


agenda_citas.getFechasSeleccionadas = ( fecha_cita, fecha_limite ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`SELECT * FROM fechas WHERE fecha BETWEEN ? AND ?;`, [ fecha_cita, fecha_limite ],
      (err, res) => {
      // VALIDAR ERRORES
      if (err) { return reject({ message: err.sqlMessage }); }
      resolve(res);
    })
  })
};

agenda_citas.deleteCita = ( id ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(` UPDATE agenda_citas SET deleted = 1 WHERE idagenda_citas = ?;`,[ id],
      (err, res) => {
      // VALIDAR ERRORES
      if (err) { return reject({ message: err.sqlMessage }); }

      if (res.affectedRows == 0) { return reject({ message: 'DATO NO ENCONTRADO' }) }

      resolve({ id });
    })
  })
};


agenda_citas.citasMayoActual = ( id_terapeuta, id_paciente, hora_inicio, hora_final, fecha_cita, idservicios ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`SELECT * FROM agenda_citas
      WHERE id_terapeuta = ? AND id_paciente = ? AND hora_inicio = ? AND hora_final = ?
      AND fecha_cita >= ? AND idservicios = ?
      AND estatus = 1;`, [ id_terapeuta, id_paciente, hora_inicio, hora_final, fecha_cita, idservicios ],
      (err, res) => {
      // VALIDAR ERRORES
      if (err) { return reject({ message: err.sqlMessage }); }
      resolve(res);
    })
  })
};


agenda_citas.citasTodas = (id_terapeuta, id_paciente, hora_inicio, hora_final, idservicios ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`SELECT * FROM agenda_citas
      WHERE id_terapeuta = ? AND id_paciente = ? AND hora_inicio = ? AND hora_final = ?
      AND idservicios = ?
      AND estatus = 1;`, [id_terapeuta, id_paciente, hora_inicio, hora_final, idservicios ],
      (err, res) => {
      // VALIDAR ERRORES
      if (err) { return reject({ message: err.sqlMessage }); }
      resolve(res);
    })
  })
};

module.exports = agenda_citas;