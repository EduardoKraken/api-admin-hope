const { result } = require("lodash");
const sqlERP     = require("../db.js");


//const constructor
const objetivos = (objetivos) => {};

// Agregar datos usuario
objetivos.addObjetivo = ( o ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`INSERT INTO objetivos_paciente( objetivo, idterapeuta, idpaciente, estatus, fecha_meta ) VALUES( ?, ?, ?, ?, ? )`,
      [ o.objetivo, o.idterapeuta, o.idpaciente, o.estatus, o.fecha_meta ], (err, res) => {

      // VALIDAR ERRORES
      if (err) { return reject({ message: err.sqlMessage }); }

      // RESPUESTA AL CONTROLADOR
      return resolve({ id: res.insertId, ...o });
    });
  })
};

objetivos.getObjetivosId = ( idpaciente ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`SELECT * FROM objetivos_paciente WHERE deleted = 0 AND idpaciente = ?;`,[ idpaciente ],(err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res);
    });
  })
};


objetivos.updateObjetivo = ( o, id ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(` UPDATE objetivos_paciente SET objetivo = ?, idterapeuta = ?, idpaciente = ?, estatus = ?, fecha_meta = ?, deleted = ?
      WHERE idobjetivos_paciente = ?`, [ o.objetivo, o.idterapeuta, o.idpaciente, o.estatus, o.fecha_meta, o.deleted, id],
      (err, res) => {
      // VALIDAR ERRORES
      if (err) { return reject({ message: err.sqlMessage }); }

      if (res.affectedRows == 0) { return reject({ message: 'DATO NO ENCONTRADO' }) }

      resolve({ id, ...o });
    })
  })
};

module.exports = objetivos;