const { result } = require("lodash");
const sqlERP     = require("../db.js");


//const constructor
const evidencia_cita = (evidencia_cita) => {};

// Agregar datos usuario
evidencia_cita.addEvidenciaCita = ( idagenda_cita, archivo, extension ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`INSERT INTO evidencia_cita( idagenda_cita, archivo, extension ) VALUES( ?, ?, ? )`,
      [ idagenda_cita, archivo, extension ], (err, res) => {

      // VALIDAR ERRORES
      if (err) { return reject({ message: err.sqlMessage }); }

      // RESPUESTA AL CONTROLADOR
      return resolve({ id: res.insertId, idagenda_cita, archivo, extension });
    });
  })
};

evidencia_cita.getEvidenciaCitas = ( id ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`SELECT * FROM evidencia_cita WHERE deleted = 0 AND idagenda_cita IN ( ? );`,
      [ id ],(err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res );
    });
  })
};

module.exports = evidencia_cita;