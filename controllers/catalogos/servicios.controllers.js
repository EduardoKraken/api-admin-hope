const servicios   = require("../../models/catalogos/servicios.model.js");

// AGREGAR NIVEL
exports.addServicios = async (req, res) => {
  try{

    // vALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!req.body || Object.keys(req.body).length === 0) {
      return res.status( 400 ).send({ message: 'El Contenido no puede estar vacio' });
    }
    
    // AGREGAR EL NIVEL
    const servicioAgregado  = await servicios.addServicios( req.body ).then( response => response )

    // MENSAJE DE RESPUESTA AL USUARIO
    res.send({ message: 'Nivel generado correctamente', servicioAgregado })

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } )
  }
}

// OBTENER TODOS LOS NIVELES
exports.getServicios = async (req, res) => {
  try{
    
    // OBTENER TODOS LOS NIVELES MENOS LOS ELIMINADOS
    const serviciosAll  = await servicios.getServicios( ).then( response => response )

    // RESPUESTA AL USUARIO
    res.send( serviciosAll )

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } )
  }
};

// ACTUALIZAR EL NIVEL O BIEN ELIMINARLO
exports.updateServicios = async (req, res) => {
  try{
    // Desestrucutramos los datos
    const { id } = req.params

    // vALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!req.body || Object.keys(req.body).length === 0 || !id ) {
      return res.status( 400 ).send({ message: 'El Contenido no puede estar vacio' });
    }

    // una vez enviado, hay que actualizar la contraseñ
    const servicioActualizado = await servicios.updateServicios( req.body, id ).then( response=> response )

    res.send({ message: 'Nivel actualizado correctamente' })

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } )
  }
};

// OBTENER SOLO LOS NIVELES ACTIVOS
exports.getServiciosActivos = async (req, res) => {
  try{

    // OBTENER TODOS LOS NIVELES MENOS LOS ELIMINADOS
    const serviciosAll  = await servicios.getServiciosActivos( ).then( response => response )

    // RESPUESTA AL USUARIO
    res.send( serviciosAll )

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } )
  }
};
