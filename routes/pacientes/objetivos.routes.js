module.exports = app => {
  const objetivos = require('../../controllers/pacientes/objetivos.controllers')
  /*
    Todas las rutas aquí creadas estarán dirigidas hacia los usuarios
  */ 

  app.post("/objetivos.add",          objetivos.addObjetivo);
  app.get("/objetivos.get/:id",       objetivos.getObjetivosId);
  app.put("/objetivos.update/:id",    objetivos.updateObjetivo);

};