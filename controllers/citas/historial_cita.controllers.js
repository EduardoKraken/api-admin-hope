const historial_cita    = require("../../models/citas/historial_cita.model.js");
const evidencia_cita    = require("../../models/citas/evidencia_cita.model.js");
const agenda_citas      = require("../../models/citas/agenda_citas.model.js");

const config            = require("../../config/index.js");
const md5               = require('md5');
const { v4: uuidv4 }    = require('uuid')


// AGREGAR UN HISTORIAL DE UNA CITA
exports.addHistorialCita = async (req, res) => {
  try{

    const { fotos, idagenda_cita } = req.body

    // vALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!req.body || Object.keys(req.body).length === 0) {
      return res.status( 400 ).send({ message: 'El Contenido no puede estar vacio' });
    }
    
    // HISTORIAL GENERADO
    const historialGenerado  = await historial_cita.addHistorialCita( req.body ).then( response => response )

    const updateCita         = await agenda_citas.citaCumplida( idagenda_cita ).then( response => response )

    for( const i in fotos ){
      const { archivo, extension } = fotos[i]
      const addEvidencia = await evidencia_cita.addEvidenciaCita( idagenda_cita, archivo, extension ).then( response => response )
    }

    // MENSAJE DE RESPUESTA AL Agenda
    res.send({ message: 'Historial generado correctamente', historialGenerado })

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } )
  }
}

// ACTUALIZAR EL INFORME DE LA CITA
exports.updateHistorialCita = async (req, res) => {
  try{

    // Desestrucutramos los datos
    const { id } = req.params

    // vALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!req.body || Object.keys(req.body).length === 0 || !id ) {
      return res.status( 400 ).send({ message: 'El Contenido no puede estar vacio' });
    }

    // ACTUALIZAR EL HISTORIAL DEL PACIENTE
    const historialActualizado = await historial_cita.updateHistorialCita( req.body, id ).then( response=> response )

    res.send({ message: 'Historial actualizado correctamente' })

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } )
  }
};

