module.exports = app => {
  const grupos = require('../../controllers/catalogos/grupos.controllers')
  /*
    Todas las rutas aquí creadas estarán dirigidas hacia los grupos
  */ 

  app.post("/grupos.add",                     grupos.addGrupo);
  app.get("/grupos.get",                      grupos.getGrupos);
  app.put("/grupos.update/:id",               grupos.updateGrupos);
  app.get("/grupos.activos",                  grupos.getGruposActivos);
  app.post("/grupos.alumnos",                 grupos.getGruposAlumnos);
  app.post("/grupos.add.alumno",              grupos.addAlumnoGrupo);
  app.get("/grupos.teachers/:id",             grupos.getGruposTeacher);
  app.post("/grupos.asistencias",             grupos.getAsistencias);
  app.post("/grupos.asistencias.add",         grupos.addAsistencias);
  app.post("/grupos.asistencias.add.reporte", grupos.addReporte);

  // ALUMNOS Y TUTORES
  app.get("/alumnos.tutor/:id",               grupos.getAlumnosTutor)
  app.post("/reporte.alumno",                 grupos.gerReporteAlumno);
};