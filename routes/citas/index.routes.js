module.exports = app => {
  require("./configuracion_terapeuta.routes")(app);
  require("./agenda_citas.routes")(app);
  require("./historial_cita.routes")(app);
  require("./evidencia_cita.routes")(app);
};