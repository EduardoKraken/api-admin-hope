module.exports = app => {
  const evidencia_cita = require('../../controllers/citas/evidencia_cita.controllers')
  /*
    Todas las rutas aquí creadas estarán dirigidas hacia los usuarios
  */ 

  app.post("/evidencia.cita.add",          evidencia_cita.addEvidenciaCita);
  app.put("/evidencia.cita.update/:id",    evidencia_cita.updateEvidenciaCita);

};