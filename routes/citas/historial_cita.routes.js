module.exports = app => {
  const historial_cita = require('../../controllers/citas/historial_cita.controllers')
  /*
    Todas las rutas aquí creadas estarán dirigidas hacia los usuarios
  */ 

  app.post("/historial.cita.add",          historial_cita.addHistorialCita);
  app.put("/historial.cita.update/:id",    historial_cita.updateHistorialCita);
};