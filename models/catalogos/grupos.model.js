const { result } = require("lodash");
const sqlERP     = require("../db.js");


//const constructor
const grupos = (grupos) => {};

// Agregar datos usuario
grupos.addGrupo = ( c ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`INSERT INTO grupos( grupo, comentarios, hora_inicio, hora_final, lunes, martes, miercoles, jueves, viernes, sabado, domingo, idmaestros ) 
    	VALUES( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? )`, [ c.grupo, c.comentarios, c.hora_inicio, c.hora_final, c.lunes, c.martes, c.miercoles, c.jueves, c.viernes, c.sabado, c.domingo, c.idmaestros ], (err, res) => {

      // VALIDAR ERRORES
      if (err) { return reject({ message: err.sqlMessage }); }

      // RESPUESTA AL CONTROLADOR
      return resolve({ id: res.insertId, ...c });
    });
  })
};


grupos.getGrupos = ( ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`SELECT g.*, CONCAT(t.nombres, " ", t.apellido_paterno, " ", t.apellido_materno) AS maestro FROM grupos g
			LEFT JOIN usuarios t ON t.idusuarios = g.idmaestros
			WHERE g.deleted = 0;`, (err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res );
    });
  })
};

grupos.updateGrupos = ( c, id ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(` UPDATE grupos SET grupo = ?, comentarios = ?, hora_inicio = ?, hora_final = ?, lunes = ?, martes = ?, 
    	miercoles = ?, jueves = ?, viernes = ?, sabado = ?, domingo = ?, idmaestros = ? WHERE idgrupos = ?;`, 
      [ c.grupo, c.comentarios, c.hora_inicio, c.hora_final, c.lunes, c.martes, c.miercoles, c.jueves, c.viernes, c.sabado, c.domingo, c.idmaestros, id],
      (err, res) => {
      // VALIDAR ERRORES
      if (err) { return reject({ message: err.sqlMessage }); }

      if (res.affectedRows == 0) { return reject({ message: 'DATO NO ENCONTRADO' }) }

      resolve({ id, ...c });
    })
  })
};

grupos.getGruposActivos = ( ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`SELECT * FROM grupos WHERE deleted = 0;`, (err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res );
    });
  })
};

grupos.getGruposAlumnos = ( idgrupos ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`SELECT ga.*, 
			CONCAT(a.nombres, " ", a.apellido_paterno, " ", a.apellido_materno) AS alumno FROM grupo_alumnos ga
			LEFT JOIN alumnos a ON a.idalumnos = ga.idalumnos
			WHERE ga.idgrupos = ?;`,[ idgrupos ],(err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res );
    });
  })
};

grupos.addAlumnoGrupo = ( c ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`INSERT INTO grupo_alumnos( idalumnos, idgrupos ) VALUES( ?, ? )`, [ c.idalumnos, c.idgrupos ], (err, res) => {

      // VALIDAR ERRORES
      if (err) { return reject({ message: err.sqlMessage }); }

      // RESPUESTA AL CONTROLADOR
      return resolve({ id: res.insertId, ...c });
    });
  })
};


grupos.getGruposTeacher = ( idmaestros ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`SELECT g.*, CONCAT(t.nombres, " ", t.apellido_paterno, " ", t.apellido_materno) AS maestro FROM grupos g
      LEFT JOIN usuarios t ON t.idusuarios = g.idmaestros
      WHERE g.deleted = 0 AND g.idmaestros = ?;`,[ idmaestros ],(err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res );
    });
  })
};

grupos.getAsistencias = ( idgrupos, fecha ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`SELECT a.*, animo, banio, comida, trabajo, detallebanio FROM asistencias a
      LEFT JOIN reporte_alumnos r ON r.idalumnos = a.idalumnos
      AND r.idgrupos = a.idgrupos
      AND r.fecha = a.fecha_asistencia
      AND r.deleted = 0
      WHERE fecha_asistencia = ? AND a.idgrupos = ?;`,[ fecha, idgrupos ],(err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res );
    });
  })
};


grupos.updateAsistencia = ( idasistencias, asistencia ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(` UPDATE asistencias SET asistencia = ? WHERE idasistencias = ?;`, [ asistencia, idasistencias ],
      (err, res) => {
      // VALIDAR ERRORES
      if (err) { return reject({ message: err.sqlMessage }); }

      if (res.affectedRows == 0) { return reject({ message: 'DATO NO ENCONTRADO' }) }

      resolve({ idasistencias, asistencia });
    })
  })
};


grupos.addASistenciaGrupo = (  idgrupos, idalumnos, fecha, asistencia  ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`INSERT INTO asistencias( idgrupos, idalumnos, fecha_asistencia, asistencia  ) VALUES( ?, ?, ?, ? )`, 
      [ idgrupos, idalumnos, fecha, asistencia ], (err, res) => {

      // VALIDAR ERRORES
      if (err) { return reject({ message: err.sqlMessage }); }

      // RESPUESTA AL CONTROLADOR
      return resolve({ id: res.insertId, idgrupos, idalumnos, fecha, asistencia  });
    });
  })
};

/****************************************************************************************/
grupos.existeReporte = ( idalumnos, idgrupos, fecha ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`SELECT * FROM reporte_alumnos a WHERE idalumnos = ? AND idgrupos = ? AND fecha = ?;`,[ idalumnos, idgrupos, fecha ],(err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res[0] );
    });
  })
};


grupos.updateReporteAlumno = ( animo, banio, comida, trabajo, detallebanio, idreporte_alumnos ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(` UPDATE reporte_alumnos SET animo = ?, banio = ?, comida = ?, trabajo = ?, detallebanio = ?
      WHERE idreporte_alumnos = ?;`, [ animo, banio, comida, trabajo, detallebanio, idreporte_alumnos ], (err, res) => {
      // VALIDAR ERRORES
      if (err) { return reject({ message: err.sqlMessage }); }

      if (res.affectedRows == 0) { return reject({ message: 'DATO NO ENCONTRADO' }) }

      resolve({ animo, banio, comida, trabajo, detallebanio, idreporte_alumnos });
    })
  })
};


grupos.addReporteAlumno = (  idalumnos, idgrupos, fecha, animo, banio, comida, trabajo, detallebanio  ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`INSERT INTO reporte_alumnos( idalumnos, idgrupos, fecha, animo, banio, comida, trabajo, detallebanio ) VALUES( ?, ?, ?, ?, ?, ?, ?, ? )`, 
      [ idalumnos, idgrupos, fecha, animo, banio, comida, trabajo, detallebanio ], (err, res) => {

      // VALIDAR ERRORES
      if (err) { return reject({ message: err.sqlMessage }); }

      // RESPUESTA AL CONTROLADOR
      return resolve({ id: res.insertId, idalumnos, idgrupos, fecha, animo, banio, comida, trabajo, detallebanio  });
    });
  })
};


/******************************************************************************************/
grupos.getAlumnosTutor = ( idtutores ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`SELECT * FROM alumnos WHERE idtutores = ?;`,[ idtutores ],(err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res );
    });
  })
};

grupos.getGruposAlumno = ( idalumnos ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`SELECT g.*, ga.idalumnos FROM grupo_alumnos ga
      LEFT JOIN grupos g ON g.idgrupos = ga.idgrupos
      WHERE ga.idalumnos IN (?);`,[ idalumnos ],(err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res );
    });
  })
};




module.exports = grupos;