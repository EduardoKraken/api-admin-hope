const objetivos       = require("../../models/pacientes/objetivos.model.js");
const { v4: uuidv4 }  = require('uuid')


// AGREGAR LA CONFIGURACION DE UN HORARIO DE UN TERAPEUTA
exports.addObjetivo = async (req, res) => {
  try{

    // vALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!req.body || Object.keys(req.body).length === 0) {
      return res.status( 400 ).send({ message: 'El Contenido no puede estar vacio' });
    }

    // Agregar el objetivo 
    const addObjetivo = await objetivos.addObjetivo( req.body ).then( response=> response )

    // MENSAJE DE RESPUESTA AL HORARIO
    res.send({ message: 'Objetivo generado correctamente' })

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } )
  }
}

// OBTENER TODAS LOS HORARIOS
exports.getObjetivosId = async (req, res) => {
  try{
    
    // Sacamos el id del paciente
    const { id } = req.params 

    // OBTENER TODAS LOS HORARIOS MENOS LOS ELIMINADOS
    const objetivosAll  = await objetivos.getObjetivosId( id ).then( response => response )

    // RESPUESTA AL HORARIO
    res.send( objetivosAll )

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } )
  }
};

// ACTUALIZAR EL HORARIO O BIEN ELIMINARLO
exports.updateObjetivo = async (req, res) => {
  try{

    // Desestrucutramos los datos
    const { id } = req.params

    // vALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!req.body || Object.keys(req.body).length === 0 || !id ) {
      return res.status( 400 ).send({ message: 'El Contenido no puede estar vacio' });
    }

    // una vez enviado, hay que actualizar la contraseñ
    const objetivoActualizado = await objetivos.updateObjetivo( req.body, id ).then( response=> response )

    res.send({ message: 'Objetivo actualizado correctamente' })

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } )
  }
};
